gitlab_http_router_dir = ${gitlab_development_root}/gitlab-http-router

.PHONY: gitlab-http-router-setup
ifeq ($(gitlab_http_router_enabled),true)
gitlab-http-router-setup: gitlab-http-router-setup-timed
else
gitlab-http-router-setup:
	@true
endif

.PHONY: gitlab-http-router-setup-run
gitlab-http-router-setup-run: gitlab-http-router/.git gitlab-http-router-common-setup

gitlab-http-router/.git:
	$(Q)support/component-git-clone ${git_params} ${gitlab_http_router_repo} gitlab-http-router

.PHONY: gitlab-http-router-common-setup
gitlab-http-router-common-setup: gitlab-http-router-npm-install

.PHONY: gitlab-http-router-npm-install
gitlab-http-router-npm-install:
	@echo
	@echo "${DIVIDER}"
	@echo "Performing npm steps for ${gitlab_http_router_dir}"
	@echo "${DIVIDER}"
	$(Q)cd ${gitlab_http_router_dir} && npm install

.PHONY: gitlab-http-router-update
ifeq ($(gitlab_http_router_enabled),true)
gitlab-http-router-update: gitlab-http-router-update-timed
else
gitlab-http-router-update:
	@true
endif

.PHONY: gitlab-http-router-update-run
gitlab-http-router-update-run: gitlab-http-router/.git/pull gitlab-http-router-common-setup

.PHONY: gitlab-http-router/.git/pull
gitlab-http-router/.git/pull: gitlab-http-router/.git
	@echo
	@echo "${DIVIDER}"
	@echo "Updating ${gitlab_http_router_dir}"
	@echo "${DIVIDER}"
	$(Q)support/component-git-update gitlab_http_router gitlab-http-router main main
