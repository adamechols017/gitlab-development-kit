# GitLab AI Gateway

Configure the [GitLab AI Gateway](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist)
to run locally in GDK.

This installation method is a simpler alternative to
[manually cloning, installing, and running the AI Gateway locally](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist#how-to-run-the-server-locally).

## Prerequisites

- Access to [Google Cloud](https://docs.gitlab.com/ee/development/ai_features/#google-cloud-vertex)
- Access to [Anthropic API](https://docs.gitlab.com/ee/development/ai_features/#anthropic)

## Setup Instructions

1. Run `gdk config set gitlab_ai_gateway.enabled true`.
1. Run `gdk update`. This clones AI Gateway repository to `<GDK-root>/gitlab-ai-gateway` and install the dependencies.
1. Add your `ANTHROPIC_API_KEY` to your GDK's `gitlab-ai-gateway/.env` file
1. To ensure you're authenticated with the gcloud CLI, run `gcloud auth application-default login`.

1. (Optional) Set additional environment variables.

   1. Uncomment or add the following variables in the `gitlab-ai-gateway/.env` file for all debugging insights.
   You may need to adjust the filepath (remove `..`) for this log to show in the `gitlab-ai-gateway` root directory.

   ```plaintext
   AIGW_LOGGING__LEVEL=debug
   AIGW_LOGGING__FORMAT_JSON=false
   AIGW_LOGGING__TO_FILE=../modelgateway_debug.log
   ```

   1. To enable hot reload, set the `AIGW_FASTAPI__RELOAD` environment variable to `true` in the `.env` file.

1. Run `gdk start gitlab-ai-gateway`. (Or `gdk restart` for a completely fresh run of all applications.)

1. To verify that the GitLab AI Gateway started, go to the OpenAPI playground in AI Gateway.

   To get the URL, run `gdk status`. The output includes the address of the AI Gateway. For example:

   ```shell
   $ gdk status
   run: <your-gdk-directory>/services/gitlab-ai-gateway: (pid 96182) 158s, normally down; run: log: (pid 34280) 5556s

   <snip>

   => GitLab AI Gateway is available at http://gdk.test:5052/docs.
   ```

1. To watch the status of the service:

   - run `gdk tail gitlab-ai-gateway`

     OR

   - run:

      ```shell
      tail -f modelgateway_debug.log | fblog -a prefix -a suffix -a current_file_name -a suggestion -a language -a input -a parameters -a score -a exception
      ```

Alternatively, you can run `poetry run ai_gateway` at `<GDK-root>/gitlab-ai-gateway` to run only AI Gateway.
This is the primary way to develop and debug features in AI Gateway.

For more information, see the
[AI features based on 3rd-party integrations](https://docs.gitlab.com/ee/development/ai_features/index.html)
page.
